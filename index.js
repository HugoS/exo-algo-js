// TESTS
describe('NIVEAU I', function() {
  require('./tests/prefix')
  require('./tests/max')
  require('./tests/delete-key')
  require('./tests/contains-equals')
});

describe('NIVEAU II', function() {
  // require('./tests/truncate')
  require('./tests/max-full')
  require('./tests/object-keys')
  // require('./tests/sort')
});

// describe('NIVEAU III', function () {
//   require('./tests/flatten')
//   require('./tests/map')
// })

describe('CLASS', function() {
  require('./tests/class-easy')
  require('./tests/class-medium')
});