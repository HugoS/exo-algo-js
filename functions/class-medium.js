class Vehicule {
  constructor(model, price) {
    this.model = model
    this.price = price
    
  }
}

class Car extends Vehicule {
  constructor(model, price) {
    super(model, price)
    
  }
  start() {
    return 'La voiture démarre'
  }
  speedUp() {
    return 'La voiture accélére'
  }
  toString() {
    let string = `Model: ${this.model}, Price: ${this.price}`
    return string
  }
}
class Truck extends Vehicule {
  constructor(model,price){
    super(model, price)
  }
  start() {
    return 'Le camion démarre'
  }
  speedUp() {
    
    return 'Le camion accélére'
  }
  toString() {
    let string = `Model: ${this.model}, Price: ${this.price}`
    return string
  }
}

module.exports = {Vehicule, Car, Truck}