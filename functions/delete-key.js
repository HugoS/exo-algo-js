function deleteKey(object, key) {
  if(!key && !object) return false
  if(!key || !object) return false
  delete object[key]
  return object
}

module.exports = deleteKey