function maxFullDifference(array) {
  let bDiff=0
  if ( !array || array.length === 0 ) return false;
  for ( let i = 0; i < array.length ; i++ ){
    for ( let j = i + 1; j < array.length ; j++ ) {
      calDiff= Math.abs((array[i]-array[j]))
      if(bDiff < calDiff){
        bDiff=calDiff
      }
      
    }
  }
  return bDiff
}

module.exports = maxFullDifference